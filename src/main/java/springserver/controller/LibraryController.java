package springserver.controller;

import java.util.concurrent.CompletableFuture;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;
import springserver.resources.Library;
import springserver.service.LibraryService;

@Slf4j
@RestController
@RequestMapping(LibraryController.MAPPING)
public class LibraryController {

    public static final String MAPPING ="/libraries";

    @Autowired
    private LibraryService libraryService;

    @Autowired
    @Qualifier("rest-thread-pool") // graceful shutdown
    private ThreadPoolTaskScheduler threadPool;

    @GetMapping
    public CompletableFuture<Library> getLibrary() {
        log.info("Get request!");
        return CompletableFuture.supplyAsync(() -> execute(), threadPool);
    }

    private Library execute() {
        Library randomLibrary = libraryService.createRandomLibrary();
        log.info("Send answer!");
        return randomLibrary;
    }
}
