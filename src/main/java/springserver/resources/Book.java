package springserver.resources;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor // framework
@NoArgsConstructor // framework
public class Book {
    private String title;
    private int pages;
}
