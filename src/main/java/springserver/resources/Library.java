package springserver.resources;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor // framework
@NoArgsConstructor // framework
public class Library {
    private List<Book> books;
}
