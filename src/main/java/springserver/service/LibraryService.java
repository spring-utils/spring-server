package springserver.service;

import static java.util.Arrays.asList;

import java.util.Random;

import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import springserver.resources.Book;
import springserver.resources.Library;

@Slf4j
@Service
public class LibraryService {

    private final Random random = new Random();

    public Library createRandomLibrary() {

        int waiting = random.nextInt(1000);

        sleep(waiting);

        Book book1 = new Book("Title_1", random.nextInt(5) + 1);
        Book book2 = new Book("Title_2", random.nextInt(8) + 1);

        Library library = new Library(asList(book1, book2));
        log.debug(library.toString());

        return library;
    }

    private void sleep(int waiting) {
        log.debug("Wait: " + waiting + " ms");
        try {
            Thread.sleep(waiting);
        }
        catch(InterruptedException e) {
            log.error("Cannot sleep....", e);
        }
    }
}
