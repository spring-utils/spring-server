package springserver.controller;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.asyncDispatch;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import springserver.resources.Book;
import springserver.resources.Library;
import springserver.service.JsonSerializer;
import springserver.service.LibraryService;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class LibraryControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private LibraryService libraryService;

    @Test
    public void test()throws Exception {

        Book book1 = new Book("Title_1", 1);
        Book book2 = new Book("Title_2", 2);
        Library library = new Library(asList(book1, book2));
        when(libraryService.createRandomLibrary()).thenReturn(library);


        String currentUrl = LibraryController.MAPPING;
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(currentUrl)
                        .content(MediaType.APPLICATION_JSON_UTF8_VALUE);

        MvcResult mvcResult = mockMvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();
        ResultActions perform = mockMvc.perform(asyncDispatch(mvcResult));

        String content = perform.andReturn().getResponse().getContentAsString();

        Library library2 = JsonSerializer.fromJson(content, library.getClass());

        assertThat(library).isEqualTo(library2);
    }
}
